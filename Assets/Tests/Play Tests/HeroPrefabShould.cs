using System.Collections;
using System.Collections.Generic;
using ViewModel;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;
using Components;
using CharacterController = Components.CharacterController;
using NSubstitute;

public class HeroPrefabShould {

    public const string HERO_PREFAB_PATH = "Hero";

    private GameObject heroPrefab;
    private GameObject heroInstance;

    private CharacterController characterController;
    private CharacterData characterData;
    private CharacterHealthDisplay characterHealthDisplay;
    private CharacterMaxHealthDisplay characterMaxHealthDisplay;
    private CharacterAliveDisplay characterAliveDisplay;
    private CharacterAnimationDisplay characterAnimationDisplay;

    [SetUp]
    public void SetUp() {
        //SceneManager.LoadScene("Test Scene");
        heroPrefab = Resources.Load<GameObject>(HERO_PREFAB_PATH);
        heroInstance = GameObject.Instantiate(heroPrefab);

        if (heroInstance != null) {
            characterController = heroInstance.GetComponent<CharacterController>();
            if (characterController != null) {
                characterController.ChangeCharacter(ScriptableObject.Instantiate(characterController.Character));
                characterData = characterController.Character;
                characterHealthDisplay = characterController.CharacterHealthDisplay;
                characterMaxHealthDisplay = characterController.CharacterMaxHealthDisplay;
                characterAliveDisplay = characterController.CharacterAliveDisplay;
                characterAnimationDisplay = characterController.CharacterAnimationDisplay;
            }
        }
    }

    [TearDown]
    public void TearDown() {
        GameObject.Destroy(heroInstance);
    }

    [Test]
    public void exist_at_expected_path() {
        Assert.IsNotNull(heroPrefab);
    }

    [Test]
    public void contain_character_controller() {
        Assert.IsNotNull(characterController);
    }

    [Test]
    public void contain_character_data() {
        Assert.IsNotNull(characterData);
    }

    [Test]
    public void contain_character_health_display() {
        Assert.IsNotNull(characterHealthDisplay);
        Assert.IsNotNull(characterHealthDisplay.Character);
        Assert.AreEqual(characterHealthDisplay.Character,characterData);
        Assert.IsNotNull(characterHealthDisplay.CharacterHealthTxt);
    }

    [Test]
    public void contain_character_max_health_display() {
        Assert.IsNotNull(characterMaxHealthDisplay);
        Assert.IsNotNull(characterMaxHealthDisplay.Character);
        Assert.AreEqual(characterMaxHealthDisplay.Character, characterData);
        Assert.IsNotNull(characterMaxHealthDisplay.CharacterMaxHealthTxt);
    }

    [Test]
    public void contain_character_alive_display() {
        Assert.IsNotNull(characterAliveDisplay);
        Assert.IsNotNull(characterAliveDisplay.Character);
        Assert.AreEqual(characterAliveDisplay.Character, characterData);
        Assert.IsNotNull(characterAliveDisplay.AliveImg);
    }

    [Test]
    public void contain_character_animation_display() {
        Assert.IsNotNull(characterAnimationDisplay);
        Assert.IsNotNull(characterAnimationDisplay.Character);
        Assert.AreEqual(characterAnimationDisplay.Character, characterData);
        Assert.IsNotNull(characterAnimationDisplay.Animator);
    }

    [Test]
    public void change_character_data() {
        CharacterData newCharacterData = ScriptableObject.Instantiate(characterData);

        characterController.ChangeCharacter(newCharacterData);

        Assert.AreEqual(newCharacterData, characterController.Character);
        Assert.AreEqual(newCharacterData, characterHealthDisplay.Character);
        Assert.AreEqual(newCharacterData, characterMaxHealthDisplay.Character);
        Assert.AreEqual(newCharacterData, characterAliveDisplay.Character);
    }

    [Test]
    public void change_health_display_when_health_changes() {
        string prevHealth = characterHealthDisplay.HealthDisplayed;

        characterData.Health.Value -= 5;
        string newHealth = characterHealthDisplay.HealthDisplayed;

        Assert.AreNotEqual(newHealth, prevHealth);
    }

    [Test]
    public void change_max_health_display_when_health_changes() {
        string prevMaxHealth = characterMaxHealthDisplay.MaxHealthDisplayed;

        characterData.MaxHealth.Value -= 5;

        string newMaxHealth = characterMaxHealthDisplay.MaxHealthDisplayed;

        Assert.AreNotEqual(newMaxHealth, prevMaxHealth);
    }

    [Test]
    public void change_alive_display_when_hero_dies() {
        Color prevColor = characterAliveDisplay.currentColor;

        characterData.ReceiveDamage(characterData.Health.Value);
        Color newColor = characterAliveDisplay.currentColor;

        Assert.AreNotEqual(prevColor, newColor);
    }

    [Test]
    public void trigger_animation_when_hero_attacks() {
        bool prevState = characterAnimationDisplay.Animator.GetBool("Attack");
        CharacterData otherCharacter = ScriptableObject.CreateInstance<CharacterData>();

        characterData.Attack(otherCharacter);
        bool newState = characterAnimationDisplay.Animator.GetBool("Attack");

        Assert.AreNotEqual(prevState, newState);
    }

    [Test]
    public void trigger_animation_when_hero_heals() {
        bool prevState = characterAnimationDisplay.Animator.GetBool("Heal");
        CharacterData otherCharacter = ScriptableObject.CreateInstance<CharacterData>();

        characterData.Heal(otherCharacter);
        bool newState = characterAnimationDisplay.Animator.GetBool("Heal");

        Assert.AreNotEqual(prevState, newState);
    }

}
