using System.Collections;
using System.Collections.Generic;
using ViewModel;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class CharacterShould
{
    CharacterData character1;
    CharacterData character2;

    [SetUp]
    public void SetUp() {
        character1 = ScriptableObject.CreateInstance<CharacterData>();
        character2 = ScriptableObject.CreateInstance<CharacterData>();
        character1.CharacterAttack = ScriptableObject.CreateInstance<CharacterAttack>();
        character1.CharacterAttack.DamageAmount.Value = 5;
        character1.CharacterHeal = ScriptableObject.CreateInstance<CharacterHeal>();
        character1.CharacterHeal.HealAmount.Value = 5;
    }

    [Test]
    public void HaveAnAttack() {
        Assert.IsNotNull(character1.CharacterAttack);
    }

    [Test]
    public void StartWith1000HealthByDefault() {
        int health = character1.Health.Value;

        Assert.AreEqual(1000, health);
    }

    [Test]
    public void StartWithLevel1ByDefault() {
        int level = character1.Level.Value;

        Assert.AreEqual(1, level);
    }

    [Test]
    public void StartAliveByDefault() {
        bool alive = character1.Alive.Value;

        Assert.IsTrue(alive);
    }

    [Test]
    public void DealDamageToOtherCharacters() {
        int char2HealthPreviousToAttack = character2.Health.Value;

        character1.Attack(character2);

        Assert.Less(character2.Health.Value, char2HealthPreviousToAttack);
    }

    [Test]
    public void DieIfHealthBecomes0OrLessAfterBeingAttacked() {
        character2.Health.Value = character1.CharacterAttack.DamageAmount.Value;

        character1.Attack(character2);

        Assert.IsFalse(character2.Alive.Value);
    }

    [Test]
    public void HealCharacters() {
        int character2HealthPreviousToHeal = 500;
        character2.Health.Value = character2HealthPreviousToHeal;

        character1.Heal(character2);

        Assert.Greater(character2.Health.Value, character2HealthPreviousToHeal);
    }

    [Test]
    public void NotHealDeadCharacters() {
        int character2PreviousHealth = 0;
        character2.Health.Value = character2PreviousHealth;
        character2.Alive.Value = false;

        character1.Heal(character2);

        Assert.AreEqual(character2.Health.Value, character2PreviousHealth);
    }

    [Test]
    public void NotHealOverMaxHealth() {
        int character2PreviousHealth = character2.MaxHealth.Value;
        character2.Health.Value = character2PreviousHealth;

        character1.Heal(character2);

        Assert.AreEqual(character2.Health.Value, character2PreviousHealth);
    }
}