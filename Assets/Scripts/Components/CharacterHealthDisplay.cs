using ViewModel;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UniRx;
using System;
namespace Components {
    public class CharacterHealthDisplay : MonoBehaviour {
        [SerializeField] private CharacterData character;
        public CharacterData Character {
            get => character;
            set { 
                character = value;
                character.Health
                    .Subscribe(UpdateHealth)
                    .AddTo(this);
            }
        }
        [SerializeField] private TextMeshProUGUI characterHealthTxt;
        public TextMeshProUGUI CharacterHealthTxt => characterHealthTxt;
        public string HealthDisplayed => characterHealthTxt.text;

        void Awake() {
            character.Health
                .Subscribe(UpdateHealth)
                .AddTo(this);
        }

        private void UpdateHealth(int characterHealth) {
            characterHealthTxt.text = characterHealth.ToString();
        }
    }
}