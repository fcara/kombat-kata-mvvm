using ViewModel;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Components {
    public class CharacterController : MonoBehaviour {
        [SerializeField] private CharacterData character;
        public CharacterData Character => character;

        [SerializeField] private CharacterHealthDisplay characterHealthDisplay;
        public CharacterHealthDisplay CharacterHealthDisplay => characterHealthDisplay;
        [SerializeField] private CharacterMaxHealthDisplay characterMaxHealthDisplay;
        public CharacterMaxHealthDisplay CharacterMaxHealthDisplay => characterMaxHealthDisplay;
        [SerializeField] private CharacterAliveDisplay characterAliveDisplay;
        public CharacterAliveDisplay CharacterAliveDisplay => characterAliveDisplay;
        [SerializeField] private CharacterAnimationDisplay characterAnimationDisplay;
        public CharacterAnimationDisplay CharacterAnimationDisplay => characterAnimationDisplay;

        public void ChangeCharacter(CharacterData characterData) {
            character = characterData;
            characterHealthDisplay.Character = characterData;
            CharacterMaxHealthDisplay.Character = characterData;
            CharacterAliveDisplay.Character = characterData;
            CharacterAnimationDisplay.Character = characterData;
        }
    }
}