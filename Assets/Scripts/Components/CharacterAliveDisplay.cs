using ViewModel;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniRx;
using System;

namespace Components {
    public class CharacterAliveDisplay : MonoBehaviour {
        [SerializeField] private CharacterData character;
        public CharacterData Character {
            get => character;
            set {
                character = value;
                character.Alive
                    .Subscribe(OnAliveChanged)
                    .AddTo(this);
            }
        }
        [SerializeField] private Image aliveImg;
        public Image AliveImg => aliveImg;
        public Color currentColor => aliveImg.color;

        private Color normalColor;
        [SerializeField]
        private Color deadColor = Color.red;

        private void Awake() {
            normalColor = aliveImg.color;
            character.Alive.Subscribe(OnAliveChanged);
        }

        private void OnAliveChanged(bool alive) {
            aliveImg.color = alive ? normalColor : deadColor;
        }
    }
}