using ViewModel;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Command;

namespace Components {
    public class CharacterAttackInput : MonoBehaviour {
        [SerializeField]
        private CharacterCmdFactory cmdFactory;
        [SerializeField]
        private CharacterData characterData;

        public void OnClick(CharacterData targetCharacter) {
            cmdFactory.PerfomAttack(characterData, targetCharacter).Execute();
        }
    }
}