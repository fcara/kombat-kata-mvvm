using ViewModel;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UniRx;
namespace Components {
    public class CharacterMaxHealthDisplay : MonoBehaviour {
        [SerializeField] private CharacterData character;
        public CharacterData Character {
            get => character;
            set {
                character = value;
                character.MaxHealth
                    .Subscribe(UpdateMaxHealth)
                    .AddTo(this);
            }
        }
        [SerializeField] private TextMeshProUGUI characterMaxHealthTxt;
        public TextMeshProUGUI CharacterMaxHealthTxt => characterMaxHealthTxt;
        public string MaxHealthDisplayed => characterMaxHealthTxt.text;

        void Awake() {
            character.MaxHealth
                .Subscribe(UpdateMaxHealth)
                .AddTo(this);
        }

        private void UpdateMaxHealth(int characterMaxHealth) {
            characterMaxHealthTxt.text = characterMaxHealth.ToString();
        }
    }
}
