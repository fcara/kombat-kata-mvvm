using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ViewModel;
using UniRx;

namespace Components {
    public class CharacterAnimationDisplay : MonoBehaviour {
        [SerializeField] private CharacterData character;
        public CharacterData Character {
            get => character;
            set {
                character = value;
                Subscriptions();
            }
        }
        [SerializeField] private Animator animator;
        public Animator Animator {
            get => animator;
            set => animator = value;
        }

        public void Start() {
            Subscriptions();
        }

        private void Subscriptions() {
            character.onAttack
                .Subscribe(AnimateAttack)
                .AddTo(this);
            character.onHeal
                .Subscribe(AnimateHeal)
                .AddTo(this);
        }

        private void AnimateAttack(CharacterAttack attack) {
            animator.SetTrigger("Attack");
        }
        private void AnimateHeal(CharacterHeal heal) {
            animator.SetTrigger("Heal");
        }
    }
}