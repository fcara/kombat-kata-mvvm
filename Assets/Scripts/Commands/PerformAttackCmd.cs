﻿using ViewModel;

namespace Command {
    public class PerformAttackCmd : ICommand{

        private readonly CharacterData _characterData;
        private readonly CharacterData _attackedCharacterData;

        public PerformAttackCmd(CharacterData characterData, CharacterData attackedCharacterData) {
            _characterData = characterData;
            _attackedCharacterData = attackedCharacterData;
        }

        public void Execute() {
            _characterData.Attack(_attackedCharacterData);
        }
    }
}