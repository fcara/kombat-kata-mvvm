﻿using ViewModel;
using UnityEngine;
using System;

namespace Command {
    [CreateAssetMenu(fileName = "CharacterCmdFactory", menuName ="Command Factory/Character")]
    public class CharacterCmdFactory : ScriptableObject {
        public PerformAttackCmd PerfomAttack(CharacterData characterData, CharacterData attackedCharacter) {
            return new PerformAttackCmd(characterData, attackedCharacter);
        }

        public PerformHealCmd PerformHeal(CharacterData characterData, CharacterData targetCharacter) {
            return new PerformHealCmd(characterData, targetCharacter);
        }
    }
}