﻿using ViewModel;

namespace Command {
    public class PerformHealCmd : ICommand {

        private readonly CharacterData _characterData;
        private readonly CharacterData _healedCharacterData;

        public PerformHealCmd(CharacterData characterData, CharacterData healedCharacterData) {
            _characterData = characterData;
            _healedCharacterData = healedCharacterData;
        }

        public void Execute() {
            _characterData.Heal(_healedCharacterData);
        }
    }
}