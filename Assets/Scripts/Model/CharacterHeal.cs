﻿using UniRx;
using UnityEngine;

namespace ViewModel {
    [CreateAssetMenu()]
    public class CharacterHeal : ScriptableObject {
        public IntReactiveProperty HealAmount = new IntReactiveProperty(5);
    }
}
