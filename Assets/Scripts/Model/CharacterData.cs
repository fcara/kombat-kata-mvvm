using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

namespace ViewModel {

    [CreateAssetMenu()]
    public class CharacterData : ScriptableObject {

        public IntReactiveProperty MaxHealth = new IntReactiveProperty(1000);
        public IntReactiveProperty Health = new IntReactiveProperty(1000);
        public IntReactiveProperty Level = new IntReactiveProperty(1);
        public BoolReactiveProperty Alive = new BoolReactiveProperty(true);

        [SerializeField]
        private CharacterAttack characterAttack;
        public CharacterAttack CharacterAttack {
            get => characterAttack;
            set => characterAttack = value;
        }

        [SerializeField]
        private CharacterHeal characterHeal;
        public CharacterHeal CharacterHeal {
            get => characterHeal;
            set => characterHeal = value;
        }

        public ISubject<CharacterAttack> onAttack = new Subject<CharacterAttack>();
        public ISubject<CharacterHeal> onHeal = new Subject<CharacterHeal>();

        private void OnEnable() {
            Health.Value = MaxHealth.Value;
            Alive.Value = Health.Value > 0;
        }

        public void Attack(CharacterData otherCharacter) {
            onAttack.OnNext(characterAttack);
            otherCharacter.ReceiveDamage(CharacterAttack.DamageAmount.Value);
        }

        public void ReceiveDamage(int damage) {
            if (!Alive.Value)
                return;
            Health.Value -= damage;
            if (Health.Value <= 0)
                Alive.Value = false;
        }

        public void Heal(CharacterData otherCharacter) {
            onHeal.OnNext(characterHeal);
            if (otherCharacter.Alive.Value) {
                otherCharacter.Health.Value = Mathf.Clamp(otherCharacter.Health.Value+CharacterHeal.HealAmount.Value,0, otherCharacter.MaxHealth.Value);
            }
        }
    }
}
