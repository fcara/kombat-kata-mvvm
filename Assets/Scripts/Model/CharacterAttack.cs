﻿using UniRx;
using UnityEngine;

namespace ViewModel {
    [CreateAssetMenu()]
    public class CharacterAttack : ScriptableObject {
        public IntReactiveProperty DamageAmount = new IntReactiveProperty(5);
    }
}
